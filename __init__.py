bl_info = {
    "name": "Audio Notification",
    "description": "Plays a sound when your render finishes",
    "author": "Hugues Ross",
    "version": {1, 0, 0},
    "blender": (2, 80, 0),
    "category": "Render",
}

import os

import aud
import bpy
from bpy.app.handlers import persistent

device = aud.Device()

class AddonPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    sound_path: bpy.props.StringProperty(
        name="Notification Sound",
        description="The sound to play when the render finishes",
        subtype='FILE_PATH',
        default=os.path.join(os.path.dirname(__file__), "audio", "post_render.wav"),
    )

    def draw(self, context):
        layout = self.layout

        layout.prop(self, "sound_path", icon='SOUND')


@persistent
def post_render(self, context):
    preferences = bpy.context.preferences.addons[__name__].preferences
    device.play(aud.Sound(preferences.sound_path))

def register():
    bpy.utils.register_class(AddonPreferences)
    bpy.app.handlers.render_complete.append(post_render)

def unregister():
    bpy.utils.unregister_class(AddonPreferences)
    bpy.app.handlers.render_complete.remove(post_render)

if __name__ == "__main__":
    register()
